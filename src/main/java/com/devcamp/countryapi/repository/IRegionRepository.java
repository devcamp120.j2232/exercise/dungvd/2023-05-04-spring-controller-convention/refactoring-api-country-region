package com.devcamp.countryapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryapi.model.CRegion;

public interface IRegionRepository extends JpaRepository<CRegion, Long>{
    Long countByCountryId(long countryid);
}
