package com.devcamp.countryapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryapi.model.CCountry;
import com.devcamp.countryapi.model.CRegion;
import com.devcamp.countryapi.repository.ICountryRepository;
import com.devcamp.countryapi.repository.IRegionRepository;

@RestController
public class CRegionController {
    @Autowired
    IRegionRepository pIRegionRepository;

    @Autowired
    ICountryRepository pICountryRepository;

    //API để lấy thông tin toàn bộ region
    @CrossOrigin
    @GetMapping("/regions")
    public ResponseEntity<List<CRegion>> getAllRegions() {
        try {
            List<CRegion> listAllRegions = new ArrayList<>();
            pIRegionRepository.findAll().forEach(listAllRegions::add);
            return new ResponseEntity<>(listAllRegions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để lấy thông tin của 1 region thông qua ID
    @CrossOrigin
    @GetMapping("/regions/{id}")
    public ResponseEntity<CRegion> getRegionById(@PathVariable("id") long id) {
        try {
            Optional<CRegion> region = pIRegionRepository.findById(id);
            if(region.isPresent()) {
                return new ResponseEntity<>(region.get(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //API để tạo mới 1 region
    @CrossOrigin
    @PostMapping("/countries/{id}/regions")
    public ResponseEntity<CRegion> createRegion(@RequestBody CRegion pRegion, @PathVariable("id") long id) {
        try {
            CCountry country = pICountryRepository.findById(id).get();
            CRegion region = new CRegion();
            region.setRegionCode(pRegion.getRegionCode());
            region.setRegionName(pRegion.getRegionName());
            region.setCountry(country);
            CRegion savedRegion = pIRegionRepository.save(region);
            return new ResponseEntity<>(savedRegion, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để sửa thông tin 1 region
    @CrossOrigin
    @PutMapping("country/{countryid}/region/{regionid}")
    public ResponseEntity<CRegion> updateRegion(@RequestBody CRegion pRegion, @PathVariable("regionid") long regionid, @PathVariable("countryid") long countryid) {
        try {
            Optional<CRegion> regionData = pIRegionRepository.findById(regionid);
            CCountry country = pICountryRepository.findById(countryid).get();
            if(regionData.isPresent()) {
                CRegion region = regionData.get();
                region.setRegionCode(pRegion.getRegionCode());
                region.setRegionName(pRegion.getRegionName());
                region.setCountry(country);
                CRegion savedRegion = pIRegionRepository.save(region);
                return new ResponseEntity<>(savedRegion, HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    // API để xóa 1 reion
    @DeleteMapping("/regions/{regionid}")
    public ResponseEntity<CRegion> deleteRegion(@PathVariable("regionid") long regionid) {
        try {
            pIRegionRepository.deleteById(regionid);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra toàn bộ region của 1 country
    @CrossOrigin
    @GetMapping("/countries/{countryid}/regions")
    public ResponseEntity<List<CRegion>> getRegionsByCountryId(@PathVariable("countryid") long countryid) {
        try {
            List<CRegion> listRegionOfCountry = pICountryRepository.findById(countryid).get().getRegions();
            return new ResponseEntity<>(listRegionOfCountry, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    // API lấy ra số region của 1 country có Id
    @CrossOrigin
    @GetMapping("/countries/{countryid}/count/regions")
    public ResponseEntity<Long> countRegionsByCountryId(@PathVariable("countryid") long countryid) {
        try {
            Long count = pIRegionRepository.countByCountryId(countryid);
            return new ResponseEntity<>(count, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API kiểm tra xem region có tồn tại
    @CrossOrigin
    @GetMapping("regions/check/{regionid}")
    public ResponseEntity<Boolean> checkRegionById(@PathVariable("regionid") Long regionid) {
        try {
            Boolean isPresent = pIRegionRepository.existsById(regionid);
            return new ResponseEntity<>(isPresent, HttpStatus.OK);
            
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
