package com.devcamp.countryapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryapi.model.CCountry;
import com.devcamp.countryapi.repository.ICountryRepository;


@RestController
public class CCountryController {
    @Autowired
    ICountryRepository pICountryRepository;

    //API để lấy thông tin toàn bộ country
    @CrossOrigin
    @GetMapping("/countries")
    public ResponseEntity<List<CCountry>> getAllCountry() {
        try {
            List<CCountry> listAllCountries = new ArrayList<>();
            pICountryRepository.findAll().forEach(listAllCountries::add);
            return new ResponseEntity<>(listAllCountries, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để lấy thông tin của 1 country thông qua ID
    @CrossOrigin
    @GetMapping("/countries/{countryid}")
    public ResponseEntity<CCountry> getCountryById(@PathVariable("countryid") long countryid) {
        try {
            Optional<CCountry> country = pICountryRepository.findById(countryid);
            if(country.isPresent()) {
                return new ResponseEntity<>(country.get(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //API để tạo mới 1 country 
    @CrossOrigin
    @PostMapping("/countries")
    public ResponseEntity<CCountry> createCountry(@RequestBody CCountry pCountry) {
        try {
            CCountry country = new CCountry();
            country.setCountryCode(pCountry.getCountryCode());
            country.setCountryName(pCountry.getCountryName());
            CCountry savedCountry = pICountryRepository.save(country);
            return new ResponseEntity<>(savedCountry, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để sửa thông tin 1 country
    @CrossOrigin
    @PutMapping("/countries/{countryid}")
    public ResponseEntity<CCountry> updateCountry(@RequestBody CCountry pCountry, @PathVariable("countryid") long countryid) {
        try {
            Optional<CCountry> countryData = pICountryRepository.findById(countryid);
            if(countryData.isPresent()) {
                CCountry country = countryData.get();
                country.setCountryName(pCountry.getCountryName());
                country.setCountryCode(pCountry.getCountryCode());
                CCountry savedCountry = pICountryRepository.save(country);
                return new ResponseEntity<>(savedCountry, HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    // API để xóa 1 country
    @CrossOrigin
    @DeleteMapping("/country/{countryid}")
    public ResponseEntity<CCountry> deleteCountryById(@PathVariable("countryid") long countryid) {
        try {
            pICountryRepository.deleteById(countryid);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để xóa toàn bộ country
    @CrossOrigin
    @DeleteMapping("/countries")
    public ResponseEntity<CCountry> deleteAllCountry() {
        try {
            pICountryRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //API để đếm tổng số country
    @CrossOrigin
    @GetMapping("/count/countries")
    public ResponseEntity<Long> countCountry() {
        try {
            long countCountry = pICountryRepository.count();
            return new ResponseEntity<>(countCountry, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API check xem có tồn tại 1 country có ID như nhập vào hay không
	@CrossOrigin
    @GetMapping("check/countries/{countryid}")
    public ResponseEntity<Boolean> checkCountryById(@PathVariable("countryid") long countryid) {
        try {
            Boolean isPresent = pICountryRepository.existsById(countryid);
            return new ResponseEntity<>(isPresent, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    // API để phân trang
    @CrossOrigin
    @GetMapping("/country5")
    public ResponseEntity<List<CCountry>> getVoucherByPage(
                                        @RequestParam(name = "page", defaultValue = "1") String page,     
                                        @RequestParam(name = "size", defaultValue = "5") String size) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<CCountry> list = new ArrayList<CCountry>();
            pICountryRepository.findAll(pageWithFiveElements).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
